# Часть тестовое задания (бэкенд)

## Описание
REST API. Посты и комментарии.

## Запуск

1. Создать .env по аналогии с .env.example

2. Написать команды:

```
poetry shell
```
```
poetry install
```
```
python manage.py migrate
```
```
python manage.py createsuperuser
```
```
python manage.py runserver
```

3. Протестировать можно прямо в браузере /api/posts и т.д.