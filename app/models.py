from django.db import models
from django.contrib.auth.models import User

class Publication(models.Model):
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    
    class Meta:
        abstract = True
    

class Posts(Publication):
    title = models.CharField(max_length=255)
    
    class Meta:
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['-created_at']),
        ]

  
class Comments(Publication):
    post = models.ForeignKey(Posts, on_delete=models.CASCADE, related_name='comments')

    class Meta:
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['-created_at']),
        ]
        
    def pre_save(self, *args, **kwargs):
        print(args)
        print(kwargs)
        super().pre_save(*args, **kwargs)
        