from django.urls import path
from . import views


urlpatterns = [
    path('posts/', views.PostListCreateView.as_view()),
    path('posts/<int:id>/', views.PostRetrieveUpdateDestroyView.as_view()),
    path('posts/<int:post_id>/comments/', views.CommentsListCreateView.as_view()),
    path('posts/<int:post_id>/comments/<int:comment_id>/',
         views.CommentsRetrieveUpdateDestroyView.as_view()),
]
