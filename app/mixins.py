from django.shortcuts import get_object_or_404
from rest_framework.generics import GenericAPIView
from django.db.models import QuerySet

class BaseMultipleFieldLookupMixin:

    def get_object(self: GenericAPIView):
        queryset = self.get_queryset()             # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        filter = {}
        for field in self.lookup_fields:
            if self.kwargs.get(field): # Ignore empty fields.
                filter[field] = self.kwargs[field]
        obj = get_object_or_404(queryset, **filter)  # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj
    
    
class CommentsBaseMultipleFieldLookupMixin(BaseMultipleFieldLookupMixin):
    lookup_fields = ('post', 'id')
    
    def filter_queryset(self, queryset: QuerySet):
        return queryset.filter(post=self.kwargs['post_id'], id=self.kwargs['comment_id'])