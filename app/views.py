from rest_framework import generics
from rest_framework import permissions
from .models import Posts, Comments
from .serializers import (OnePostSerializer,
                          ManyPostSerializer,
                          CommentsSerializer,
                          serializers)
from .mixins import CommentsBaseMultipleFieldLookupMixin
from rest_framework.pagination import PageNumberPagination


class CustomPagination(PageNumberPagination):
    page_size = 1


class PostListCreateView(generics.ListCreateAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = ManyPostSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        return Posts.objects.all().order_by('-created_at')


class PostRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = OnePostSerializer
    lookup_field = 'id'
    
    def get_queryset(self):
        return Posts.objects.all().order_by('-created_at')


class CommentsListCreateView(generics.ListCreateAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = CommentsSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        return (Comments.objects
                .filter(post=self.kwargs['post_id'])
                .all()
                .order_by('-created_at'))
    
    def post(self, request, *args, **kwargs):
        post_id = kwargs.get('post_id')
        posts = Posts.objects.filter(id=post_id)
        
        if not posts.exists():
            raise serializers.ValidationError(detail="Такого поста не существует")
        
        return super().post(request, *args, **kwargs)


class CommentsRetrieveUpdateDestroyView(
    CommentsBaseMultipleFieldLookupMixin,
    generics.RetrieveUpdateDestroyAPIView
):
    permission_classes = [permissions.AllowAny]
    serializer_class = CommentsSerializer
    
    def get_queryset(self):
        return Comments.objects.order_by('-created_at').all()
    
    def put(self, request, *args, **kwargs):
        post_id = kwargs.get('post_id')
        posts = Posts.objects.filter(id=post_id)
        
        if not posts.exists():
            raise serializers.ValidationError(detail="Такого поста не существует")
        
        return super().put(request, *args, **kwargs)