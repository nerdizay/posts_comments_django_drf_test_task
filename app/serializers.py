from rest_framework import serializers
from .models import Posts, Comments, Publication, User
from django.contrib.auth.models import AnonymousUser
from rest_framework import status


class CustomPermissionError(serializers.ValidationError):
    status_code = status.HTTP_403_FORBIDDEN


class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = '__all__'
        read_only_fields = ['created_at', 'updated_at', 'author']
        abstract = True
        
    def create(self, validated_data):
        user: AnonymousUser | User = self.context['request'].user
        
        if user.is_anonymous == True:
            raise CustomPermissionError(detail="Вы не авторизованы")
        
        validated_data['author'] = user
        return super().create(validated_data)
    
    def update(self, instance: Publication, validated_data):
        user: AnonymousUser | User = self.context['request'].user
        
        if user.is_anonymous == True:
            raise CustomPermissionError(detail="Вы не авторизованы")
        
        if instance.author != user:
            raise CustomPermissionError(detail="Вы не можете редактировать, потому что вы не автор")
        
        return super().update(instance, validated_data)
    
    
class CommentsSerializer(PublicationSerializer):
    
    class Meta(PublicationSerializer.Meta):
        model = Comments
        read_only_fields = PublicationSerializer.Meta.read_only_fields + ['post']
        abstract = False
        
    def create(self, validated_data):
        post_id = self.context['request'].parser_context['kwargs']['post_id']
        post = Posts.objects.get(id=post_id)
        validated_data['post'] = post
        return super().create(validated_data)


class BasePostSerializer(PublicationSerializer):
    class Meta(PublicationSerializer.Meta):
        model = Posts
        abstract = False
        
class OnePostSerializer(BasePostSerializer):
    comments = CommentsSerializer(many=True, read_only=True)


class ManyPostSerializer(BasePostSerializer):
    comments_count = serializers.SerializerMethodField()
    
    def get_comments_count(self, obj: Posts):
        return obj.comments.count()
    
    class Meta(BasePostSerializer.Meta):
        read_only_fields = BasePostSerializer.Meta.read_only_fields + ['count']
        
        

        


        
