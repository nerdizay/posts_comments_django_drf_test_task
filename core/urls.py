from django.contrib import admin
from django.urls import path, include
from rest_framework.schemas import get_schema_view


api_urls = (
    *include('app.urls'),
    #TODO: Сюда можно добавлять другие API
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path(
        "openapi",
        get_schema_view(
            title="My API",
            description="API for all things …",
            version="1.0.0",
        ),
    ),
    path('api/', api_urls),
]